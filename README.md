# Golang database driver for Siodb

## Get the last version of the Siodb driver

```bash
cd ~/siodb-go-driver-tests
export GOPATH=~/siodb-go-driver-tests
go get bitbucket.org/siodb-squad/siodb-go-driver
go run tests.go
```

