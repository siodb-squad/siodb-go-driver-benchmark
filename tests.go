// Copyright (C) 2019-2020 Siodb GmbH. All rights reserved.
// Use of this source code is governed by a license that can be found
// in the LICENSE file.

package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"sync"
	"time"

	_ "bitbucket.org/siodb-squad/siodb-go-driver"
)

// Database connection pool.
var pool *sql.DB

type benchmark struct {
	databaseName string
	tableName    string
	tableColsDef string
	ctx          context.Context
	totalTx      int64
	totalMs      int64
}

func main() {

	// Create db connection pool to Siodb
	pool, _ := createConnectionPool()

	ctx, stop := context.WithCancel(context.Background())

	// Check db connectivity
	Ping(ctx)

	// Configure benchmark
	var b benchmark
	b.databaseName = "TEST"
	b.tableName = "T" + strconv.FormatInt(time.Now().UnixNano(), 10)
	b.tableColsDef = "name text, email text, subscription_date timestamp"
	b.ctx = ctx

	// Setup benchmark
	b.createDatabase()
	b.createTable()

	// Launch benchmark
	b.launchBenchmark("testQuery", 10, 10)
	b.launchBenchmark("testQuery", 10, 100)

	pool.Close()
	stop()

}

func (b benchmark) launchBenchmark(name string, nbThreats int, nbTxPerThreat int) {

	var wg sync.WaitGroup
	var tx, ms int64 = 0, 0
	b.totalTx, b.totalMs = 0, 0
	for i := 1; i <= nbThreats; i++ {

		wg.Add(1)

		go func() {

			switch {
			case name == "testDMLInsert":
				tx, ms = testDMLInsert(b.ctx, &wg, b.tableName, nbTxPerThreat)
				b.totalTx = b.totalTx + tx
				b.totalMs = b.totalMs + ms
			case name == "testQuery":
				tx, ms = testQuery(b.ctx, &wg, b.tableName, nbTxPerThreat)
				b.totalTx = b.totalTx + tx
				b.totalMs = b.totalMs + ms
			case name == "testQuery2":
				tx, ms = testQuery2(b.ctx, &wg, nbTxPerThreat)
				b.totalTx = b.totalTx + tx
				b.totalMs = b.totalMs + ms
			case name == "testDMLDelete":
				tx, ms = testDMLDelete(b.ctx, &wg, b.tableName, nbTxPerThreat)
				b.totalTx = b.totalTx + tx
				b.totalMs = b.totalMs + ms
			default:
				log.Fatalf("ERROR: unknown benchmark name: %s", name)
			}

		}()
	}

	wg.Wait()

	fmt.Printf("Result test: %s | %d. Elapsed time (ms) - cumulated: %d, real: %d. Executions per second: %.2f.\n",
		name,
		b.totalTx,
		b.totalMs,
		b.totalMs/int64(nbThreats),
		float64(b.totalTx)/(float64(b.totalMs/int64(nbThreats))/1000))

}

func createConnectionPool() (*sql.DB, error) {

	var err error
	pool, err = sql.Open("siodb", "siodb://root@localhost:50000?identity_file=/home/siodb/.ssh/id_rsa&trace=false")
	if err != nil {
		return pool, err
	}

	pool.SetConnMaxLifetime(0)
	pool.SetMaxIdleConns(5)
	pool.SetMaxOpenConns(8)

	return pool, nil
}

func Ping(ctx context.Context) {

	if err := pool.PingContext(ctx); err != nil {
		log.Fatalf("unable to connect to database: %v", err)
	}
}

func (b benchmark) createDatabase() {

	var name string = ""
	err := pool.QueryRowContext(b.ctx, "select name from sys.sys_databases where name = '"+strings.ToUpper(b.databaseName)+"'").Scan(&name)
	switch {
	case err == sql.ErrNoRows:
		if _, err := pool.ExecContext(b.ctx, "create database "+strings.ToUpper(b.databaseName)+" with cipher_id = 'none'"); err != nil {
			log.Fatal("An error happened: ", err)
		}
	case err != nil:
		log.Fatalf("An error happened: %v\n", err)
	case err == nil:
		break
	default:
		fmt.Println("ERROR: ", err)
	}

}

func (b benchmark) createTable() error {

	var name string
	err := pool.QueryRowContext(b.ctx, "select name from test.sys_tables where name = '"+strings.ToUpper(b.tableName)+"'").Scan(&name)
	switch {
	case err == sql.ErrNoRows:
		if _, err := pool.ExecContext(b.ctx, "create table test."+strings.ToUpper(b.tableName)+" ("+b.tableColsDef+")"); err != nil {
			log.Fatal("An error happened: ", err)
			return err
		}
	case err != nil:
		log.Fatalf("An error happened: %v\n", err)
		return err
	case err == nil:
		fmt.Println("INFO: " + strings.ToUpper(b.tableName) + " Table already exists.")
	default:
		fmt.Println("ERROR: ", err)
		return err
	}

	fmt.Printf("Table for this test created: test.%s\n", b.tableName)

	return nil

}

func testDMLInsert(ctx context.Context, wg *sync.WaitGroup, tableName string, tXPerOps int) (nbTx int64, ms int64) {

	defer wg.Done()

	start := time.Now().UnixNano() / int64(time.Millisecond)

	for i := 1; i <= tXPerOps; i++ {
		if _, err := pool.ExecContext(ctx, "insert into test."+tableName+" values ( 'Nicolas"+fmt.Sprintf("%d", i)+"', 'nicolas"+fmt.Sprintf("%d", i)+"@siopool.io' ) "); err != nil {
			log.Fatal("An error happened: ", err)
		} else {
			nbTx++
		}
	}

	return nbTx, (time.Now().UnixNano() / int64(time.Millisecond)) - start
}

func testDMLDelete(ctx context.Context, wg *sync.WaitGroup, tableName string, tXPerOps int) (nbTx int64, ms int64) {

	defer wg.Done()

	start := time.Now().UnixNano() / int64(time.Millisecond)

	for i := 1; i <= tXPerOps; i++ {
		if _, err := pool.ExecContext(ctx, "delete from test."+tableName+" where trid = "+fmt.Sprintf("%d", i)); err != nil {
			log.Fatal("An error happened: ", err)
		} else {
			nbTx++
		}
	}

	return nbTx, (time.Now().UnixNano() / int64(time.Millisecond)) - start
}

func testQuery(ctx context.Context, wg *sync.WaitGroup, tableName string, tXPerOps int) (nbTx int64, ms int64) {

	defer wg.Done()

	start := time.Now().UnixNano() / int64(time.Millisecond)

	for i := 1; i <= tXPerOps; i++ {
		rows, err := pool.QueryContext(ctx, "select email from test."+tableName+" where trid = "+fmt.Sprintf("%d", rand.Intn(tXPerOps)))
		if err != nil {
			log.Fatal(err)
		}
		nbTx++
		defer rows.Close()

		var email sql.NullString

		for rows.Next() {
			if err := rows.Scan(&email); err != nil {
				log.Fatal(err)
			}
		}
		if err := rows.Err(); err != nil {
			log.Fatal(err)
		}
	}

	return nbTx, (time.Now().UnixNano() / int64(time.Millisecond)) - start
}

func testQuery2(ctx context.Context, wg *sync.WaitGroup, tXPerOps int) (nbTx int64, ms int64) {

	defer wg.Done()

	start := time.Now().UnixNano() / int64(time.Millisecond)

	for i := 1; i <= tXPerOps; i++ {
		_, err := pool.ExecContext(ctx, "use database test")
		if err != nil {
			log.Fatal(err)
		}
		nbTx++
	}

	return nbTx, (time.Now().UnixNano() / int64(time.Millisecond)) - start
}
